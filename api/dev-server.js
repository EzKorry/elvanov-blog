const { spawn } = require("child_process");
const esbuild = require("esbuild");
const result = esbuild.build({
  bundle: true,
  entryPoints: ["./src/server.ts"],
  target: "node16",
  tsconfig: "tsconfig.json",
  outdir: "dev-build",
  platform: "node",
  sourcemap: true,
  incremental: true,
  watch: { onRebuild: (error, build) => {
    console.log('rebuilt!');
    console.log(error);
    console.log(build);
  } },
});
