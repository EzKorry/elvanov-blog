require("esbuild")
  .build({
    entryPoints: ["src/server.ts"],
    bundle: true,
    tsconfig: "tsconfig.json",
    platform: "node",
    sourcemap: true,
    outdir: "dist",
    external: [
      "express",
      "apollo-server-express",
      "compression",
      "cors",
      "helmet",
      "@elvanov-blog",
      "mongoose",
      "@graphql-tools",
      "graphql-tag",
    ],
  })
  .catch(() => process.exit(1));
