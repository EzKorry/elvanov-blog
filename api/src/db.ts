import { connect } from "mongoose";
import { getMongodbUrl } from "@elvanov-blog/common";
import { UserModel } from "./model/user";

export async function initDB(): Promise<void> {
  console.log(getMongodbUrl());
  await connect(getMongodbUrl());

  const found = await UserModel.find({
    name: "Bill",
  });
  console.log(found);
}
