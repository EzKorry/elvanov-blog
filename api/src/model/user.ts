// import {Entity, ObjectID, ObjectIdColumn, Column} from "typeorm";

// @Entity("user")
// export class User {

//     @ObjectIdColumn()
//     id!: ObjectID;

//     @Column()
//     firstName!: string;

//     @Column()
//     lastName!: string;

// }

import { Schema, model, connect } from "mongoose";

// 1. Create an interface representing a document in MongoDB.
interface User {
  name: string;
  email: string;
  avatar?: string;
}

// 2. Create a Schema corresponding to the document interface.
const schema = new Schema<User>({
  name: { type: String, required: true },
  email: { type: String, required: true },
  avatar: String,
});

// 3. Create a Model.
export const UserModel = model<User>("User", schema);


