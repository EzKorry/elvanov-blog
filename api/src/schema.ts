import { makeExecutableSchema } from "@graphql-tools/schema";
import {resolver as userResolver, typedef as userTypedef} from "./service/user"

export default makeExecutableSchema({
  typeDefs: [userTypedef],
  resolvers: [userResolver],
});