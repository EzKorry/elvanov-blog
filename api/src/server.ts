import express from "express";
import { ApolloServer } from "apollo-server-express";
import { createServer } from "http";
import compression from "compression";
import cors from "cors";
import helmet from "helmet";
import schema from "./schema";
import { asyncForEach } from "@elvanov-blog/common"
import { initDB } from "./db";

async function run() {
  const PORT = process.env.PORT || 3200;
  const app = express();
  app.use(cors()); // todo: should fix
  app.use(helmet());
  app.use(compression());
  const server = new ApolloServer({
    schema,
  });
  await Promise.all(
    asyncForEach([1, 2, 3], async (value) => {
      console.log(value);
    })
  );

  await initDB(); 

  await server.start();
  server.applyMiddleware({ app, path: "/graphql" });
  const httpServer = createServer(app);
  httpServer.listen({ port: PORT }, (): void => {
    console.log(
      `🚀GraphQL-Server is running on http://localhost:${PORT}/graphql`
    );
  });
}
run().catch((e) => {
  console.error(e);
});