import { gql } from "graphql-tag";

export const typedef = gql`
  type User {
    name: String
  }
  type Query {
    getAllUsers: [User]
  }
`;

export const resolver = {
  Query: {
    getAllUsers: async (_: any, args: any) => {
      const mockUsers = [{ name: "xyz" }, { name: "abc" }];
      return mockUsers;
    },
  },
};
