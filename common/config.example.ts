import { IAppConfig, IAppConfigItem } from "./type";

const config: IAppConfig = {
  ci: {},
  development: {
    mongodbUrl:
      "",
  },
  production: {},
  stage: {},
};

export function getConfig(): IAppConfigItem {
  if (process.env.NODE_ENV === "production") {
    return config.production;
  }
  return config.development;
}

export function getMongodbUrl(): string {
  return getConfig().mongodbUrl || '';
}