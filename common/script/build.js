const glob = require("glob");
glob("**/!(*.d).ts", (err, matches) => {
  require("esbuild")
    .build({
      entryPoints: ["index.ts"],
      tsconfig: "tsconfig.json",
      platform: "node",
      sourcemap: true,
      bundle: true,
      outdir: ".",
    })
    .catch(() => process.exit(1));
});
