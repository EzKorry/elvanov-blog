export interface IAppConfig {
  development: IAppConfigItem;
  production: IAppConfigItem;
  ci: IAppConfigItem;
  stage: IAppConfigItem;
}

// declare module '@config' {
//   export default IAppConfig 
// }

export interface IAppConfigItem {
  "test.config"?: string;
  mongodbUrl?: string;
}

// Extend the NodeJS namespace with Next.js-defined properties
declare namespace NodeJS {
  interface Process {
    readonly browser: boolean
  }

  interface ProcessEnv {
    readonly NODE_ENV: 'development' | 'production' | 'test' | 'ci' | 'stage';
  }
}