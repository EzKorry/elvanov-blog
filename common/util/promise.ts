export function asyncMap<T, ReturnType = void>(
  array: T[],
  func: (value: T, index: number, array: T[]) => Promise<ReturnType>
) {
  const promises = array.map((value, index, arr) => {
    return func(value, index, arr);
  });
  return promises;
}

export function asyncForEach<T>(
  array: T[],
  func: (value: T, index: number, array: T[]) => Promise<void>
) {
  const promises = array.map((value, index, arr) => {
    return func(value, index, arr);
  });
  return promises;
}
