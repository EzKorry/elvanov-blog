const { Server } = require("http");

require("esbuild")
  .build({
    entryPoints: ["server.ts"],
    bundle: true,
    tsconfig: "tsconfig.json",
    platform: "node",
    target: "node16",
    sourcemap: true,
    watch: {
      onRebuild() {
        console.log("ℹ️ rebuilt");
      },
    },
    incremental: true,
    outfile: "dev-server.js",
    external: [
      "express",
      "apollo-server-express",
      "compression",
      "cors",
      "helmet",
      "@elvanov-blog",
      "mongoose",
      "@graphql-tools",
      "graphql-tag",
      "next",
    ],
  })
  .then(() => {
    console.log("✅ @elvanov-blog/web dev-server running");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
