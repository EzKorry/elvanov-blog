import next from "next";

import express from "express";
import { parse } from "url";

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const expressApp = express();

  expressApp
    .use("*", (req, res) => {
      // Be sure to pass `true` as the second argument to `url.parse`.
      // This tells it to parse the query portion of the URL.
      console.log("ho");
      const parsedUrl = parse(req.url, true);
      const { pathname, query } = parsedUrl;

      if (pathname === "/a") {
        app.render(req, res, "/a", query);
      } else if (pathname === "/b") {
        app.render(req, res, "/b", query);
      } else {
        app.renderToHTML(req, res, pathname ?? "/", query).then((html) => {
          console.log(html);
          res.send(204);
        });
        // handle(req, res, parsedUrl);
      }
    })
    .listen(3000, () => {
      console.log("> Ready on http://localhost:3000");
    });
});
